import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { global } from '../app/global';


@Injectable()
export class TestService {
    header:any;
    options:any;
    constructor(public http: Http) {
        this.header = new Headers();
        this.header.append('Accept', 'application/json');
        this.header.append('Content-Type', 'application/json');
        this.options = new RequestOptions({ headers: this.header });
    }

    testUrl(){
        return this.http.get(global.API_TEST).pipe(map((res) => res.json()));
    } 

    testPost(data){
        var data_entry = JSON.stringify(data);
        return this.http.post(global.API_TEST,data_entry,this.options).pipe(map((res) => res.json()));
    }

    getFilters(){
        return this.http.get(global.API_GET_FILTERS).pipe(map((res) => res.json()));
    }

    calculate(data){
        var data_entry = JSON.stringify(data);
        return this.http.post(global.API_CASE_POST,data_entry,this.options).pipe(map((res) => res.json()));    
    }

    getTodos(){
        return this.http.get(global.API_PLACEHOLDER).pipe(map((res) => res.json()));
    }

    getSystem(id){
        let url = global.API_GET_SYSTEM;
        url = url.replace('__id__',id); 
        return this.http.get(url,this.options).pipe(map((res) => res.json())); 
    }

    getChartData(id){
        let url = global.API_GET_CHART_DATA;
        url = url.replace('__id__',id); 
        return this.http.get(url,this.options).pipe(map((res) => res.json()));
    }

    
}
