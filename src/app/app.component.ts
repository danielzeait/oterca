import { Component, OnInit } from '@angular/core';
import { TestService } from '../providers/test.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ModalDialogComponent } from '../app/components/modal-dialog/modal-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'chart-test'; 
  classes=['nav-link first-link','nav-link active','nav-link'];
  views=[{id:0,name:'billboard'},{id:1,name:'study-cases'},{id:2,name:'password'}];
  current_view = this.views[1];
  testData:any;
  constructor( private testService:TestService,public dialog: MatDialog){
    this.testData = {
      title: 'foo',
      body: 'bar',
      userId: 1
    }
  }
  ngOnInit(){

  }

  changeView(i){
    this.current_view = this.views[i];
    this.classes = ['nav-link first-link','nav-link','nav-link'];
    if(i==0){
      this.classes[0]='nav-link first-link active-first'
    }else{
      this.classes[i]='nav-link active'    
    }
  }

  test(){
    this.testService.getSystem(7).subscribe((response)=>{
      console.log(response);
    },error=>{
      console.log(error);
    })
  }

  makePost(){
    this.testService.testPost(this.testData).subscribe((response)=>{
      console.log(response);
    },error=>{
      console.log(error);
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalDialogComponent, {
      width: '60%',
      data:{name:'hola'},
    });
  }

}
