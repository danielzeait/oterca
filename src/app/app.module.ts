import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { WizzardComponent } from './components/wizzard/wizzard.component';
import { MaterialModule } from './material'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule }  from '@angular/material';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { TableComponent } from './components/table/table.component';
import { GaugeComponent } from './components/gauge/gauge.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { GaugesModule } from '@progress/kendo-angular-gauges';
import { FormComponent } from './components/form/form.component';
import { ModalDialogComponent } from './components/modal-dialog/modal-dialog.component';

//Providers
import { TestService } from '../providers/test.service';
import { SimpleChartComponent } from './simple-chart/simple-chart.component';



@NgModule({
  declarations: [
    AppComponent,
    LineChartComponent,
    TabsComponent,
    WizzardComponent,
    TableComponent,
    GaugeComponent,
    FormComponent,
    SimpleChartComponent,
    ModalDialogComponent
  ],
  entryComponents:[
    ModalDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule, 
    HttpModule,
    MatIconModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSelectModule,
    MatGridListModule,
    MatNativeDateModule,  
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    MaterialModule,
    ChartsModule,
    GaugesModule
  ],
  providers: [
    TestService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
