import { environment } from '../environments/environment';

const server_name = environment.apiUrl;
const base = 'https://redraven.turpialtech.com';
export const global = {
    //Variables
    //Get all filters in DB
    API_GET_FILTERS: base+'/model-filter/list/',

    //Link to test
    API_TEST:base+'/test/',

    //Link to post a new filter case
    API_CASE_POST:base+'/case/',

    API_PLACEHOLDER:'https://jsonplaceholder.typicode.com/todos/1',

    //Get a system calculations 
    API_GET_SYSTEM:base+'/case/__id__/',

    //Get data points for charts
    API_GET_CHART_DATA:base+'/case/__id__/chart',


}