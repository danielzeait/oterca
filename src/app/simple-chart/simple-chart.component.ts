import { Component, OnInit, Input } from '@angular/core';
import Chart from 'chart.js';
@Component({
  selector: 'app-simple-chart',
  templateUrl: './simple-chart.component.html',
  styleUrls: ['./simple-chart.component.css']
})
export class SimpleChartComponent implements OnInit {
  @Input('color') color:number;
  @Input('data') data:any;
  lineChartColors:Array<any>;
  dataX:any=[];
  dataY:any=[];
  public chartColors = [
    {
      backgroundColor: 'rgba(0,0,0,1)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      backgroundColor: 'rgba(25,59,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      backgroundColor: 'rgba(25,59,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      backgroundColor: 'rgba(25,59,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  ]
  ngOnInit(){
    this.lineChartColors = [
      this.chartColors[this.color],
    ];
    for (let index = 0; index < this.data.length; index++) {
      this.dataY.push(this.data[index].drop_pressure);
      this.dataX.push(this.data[index].time);
    }
  }
  public MyScale = Chart.Scale.extend({
  });
  
  public lineChartData:Array<any> = [
    {data:this.dataY,label:'Serie A'}
  ]

  public lineChartLabels:Array<any> = this.dataX;
  
  public lineChartOptions:any = {
      elements: {
        line: {
            tension: 0
        }
      },
      responsive: true,
      legend:{
        position:'bottom'
      },
      scales: {
        stacked: false,
        xAxes: [{
          time:{
            //stepSize:12
          },
          ticks:{
            autoSkip:true,
            maxTicksLimit:6,
          }
        }
      ],
        yAxes: [{
          stacked: true,
          position: "left",
          id: "y-axis-0",
        }]
      }
    };

  
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
 
  public randomize():void {
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
}


