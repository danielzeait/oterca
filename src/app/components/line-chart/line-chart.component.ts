import { Component, OnInit,Input} from '@angular/core';
import { TestService } from '../../../providers/test.service';
import Chart from 'chart.js';
@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit{
  @Input('data') data:Array<Object>;
  chartData:any;
  backendData:any;
  dataX:any=[];
  dataY:any=[];
  
  
  constructor( private testService:TestService){
    
    this.chartData = {};
    
  }
  ngOnInit(){
    this.backendData = this.data;
    for (let index = 0; index < this.backendData.length; index++) {
      this.dataY.push(this.backendData[index].drop_pressure);
      this.dataX.push(this.backendData[index].time);
    }
  }


  // public lineChartData:Array<any> = [
  //   {data: [65, 59, 80, 81, 56, 55, 40,65, 59, 80, 81, 56,52,56,20,82,90], label: 'Series A'},
  //   // {data: [28, 48, 40, 19, 86, 27, 90,36, 49, 70, 88, 32,52,56,20,82,], label: 'Series B'},
  //   // {data: [18, 48, 77, 9, 100, 27, 40,58, 63, 76, 61, 15,52,56,20,82,], label: 'Series C'}
  // ];
  
  //public lineChartLabels:Array<any> = ['1', '2', '3', '4', '5', '6', '7','8', '9', '10', '11', '12','13','14', '15', '16','17'];
  public lineChartData:Array<any> = [
    {data:this.dataY,label:'Serie A'},
  ]
  public lineChartLabels:Array<any> = this.dataX;

  public lineChartOptions:any = {
    elements: {
      line: {
          tension: 0
      }
    },
    responsive: true,
    legend:{
      position:'bottom'
    },
    scales: {
      stacked: false,
      xAxes: [{
        time:{
          //stepSize:12
        },
        ticks:{
          autoSkip:true,
          maxTicksLimit:6,
        }
      }
    ],
      yAxes: [{
        stacked: true,
        position: "left",
        id: "y-axis-0",
      }]
    }
  };

  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
 
  public randomize():void {
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

  public renderData(){
    this.lineChartData = [
      {data:this.dataY,label:'Serie A'}
    ]
    this.lineChartLabels = this.dataX;
  }
}
