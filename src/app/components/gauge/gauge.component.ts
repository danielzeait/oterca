import { Component, OnInit,Input, Output,EventEmitter } from '@angular/core';
@Component({
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.css']
})
export class GaugeComponent implements OnInit {
  @Input('value') value:number;
  @Output() set = new EventEmitter<number>();;
  gaugeValue:any;
  constructor() { }

  ngOnInit() {
    this.gaugeValue = this.value;
  }

  changeValue(){
    this.set.emit(this.gaugeValue);
  }

}
