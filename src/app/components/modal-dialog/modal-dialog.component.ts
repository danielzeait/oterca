import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA} from '@angular/material';
import { TestService } from '../../../providers/test.service';

@Component({
  selector: 'app-modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.css']
})
export class ModalDialogComponent implements OnInit {
  data:any={inverse:[],poly:[]};
  constructor(public testService:TestService) { 

  }

  ngOnInit() {
    this.testService.testUrl().subscribe((response)=>{
      console.log(response);
      this.data = response;
    },error=>{
      console.log(error);
    })
  }

}
