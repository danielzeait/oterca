import { Component, OnInit, Input} from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { TestService } from '../../../providers/test.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  animations:[
    trigger('visibility', [
  		state('visible',style({
  			transform: 'translateX(0)'
  		})),
  		state('invisible',style({
  			transform: 'translateX(-200%)'
  		})),
  		transition('* => *', animate('.5s'))
  	]),
  ]
})
export class FormComponent implements OnInit {
  step:any=1;
  colors:any=[];
  footer_class:any=[];
  icon_class:any=[];
  right_texts:any=[];
  left_texts:any=[];
  current_left_text:any='';
  current_right_text:any='';
  animation_class:any='';
  animation_class_in:any='';
  visibleState:any = 'visible';
  visibleState2:any = 'invisible';
  varToggle:any=false;
  loader:any=false;
  disOptionsSelect:any;
  rows:any=[];
  btn_class=['btn-deactive','btn-deactive','btn-deactive'];
  company_data:any={};
  defaultText:any='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
  system:any={};
  current_filter:any={};
  resultsCharts:any=[];
  proposed_filter:any={};
  data:any={};
  modelFilters:any;
  chartData:any;
  showGraphics:any=false;
  showMainGraphic:any=false;
  systemTest:any;

  constructor(private testService:TestService) { 
    this.colors=['primary','secondary'
  ];
    this.right_texts = ['CUSTOMER FILTRATION SYSTEM','CURRENT FILTER','PROPOSED FILTER','COMPARE'];
    this.left_texts = ['CLIENT INFORMATION','CUSTOMER FILTRATION SYSTEM','CURRENT FILTER','PROPOSED FILTER'];
    this.current_left_text = this.left_texts[0];
    this.current_right_text = this.right_texts[0];
    this.getModelFilters();
    
    this.company_data = {
      "name":'',
      "contact":'',
      "address":'',
      "email":'',
      "phone":'',
      "zipCode":''
    }

    
    this.current_filter={
      "model_filter":0,
      "filter_name":"",
      "manufacturer":"",
      "merv_rating":"",        
      "part_number":"",
      "filter_type":'',
      "frame_type":"",
      "average_pressure_drop":0.1,
      "aditional_costs":['','','','','',''],
      "replace_cost":0,
      "annual_cost":0
    }
    
    this.proposed_filter={
      "average_pressure_drop": 0.1,
      "model_filter":0,
      "filter_cost":0,
      "replace_cost":0,
      "annual_cost":0,
      "additional_costs": 0,
      "manufacturer": "",
      "merv_rating": "blah",
      "width":this.current_filter.width,
      "height":this.current_filter.height,
      "length":this.current_filter.length
    }
 
    this.system={
      "variable_airflow":false,
      "backward_fan":false,
      "co2_cost":0,
      "forward_fan":false,
      "radial_fan":false,
      "drive_efficiency":50,
      "fan_efficiency":50,
      "motor_efficiency":50,
      "airflow_per_filter":0,
      "total_airflow ":0,
      "comments":""
    }

  }

  ngOnInit() {
    this.colors=['primary','secondary'];
    this.icon_class = ['footer-icon-active','footer-icon','footer-icon','footer-icon','footer-icon']
    this.footer_class = ['footer-text-active','footer-text','footer-text','footer-text','footer-text'];

    this.rows[0] = {title:'CURRENT FILTER'};
    this.rows[1] = {title:'CURRENT FILTER OPTIMIZED'};
    this.rows[2] = {title:'PROPOSED FILTER WITH SAME CHANGE OUT CYCLE'};
    this.rows[3] = {title:'PROPOSED FILTER WITH SAME Dp Avg'};
    this.rows[4] = {title:'PROPOSED FILTER OPTIMIZED'};
    

    // this.testService.getSystem(7).subscribe((response)=>{
    //   this.systemTest = response;
    //   console.log(this.systemTest);
    //   this.setRowsData();
    //   this.getCharts();
    // },error=>{
    //   console.log(error);
    // })
  }

  getCharts(id){
    this.testService.getChartData(id).subscribe((response)=>{
      console.log(response);
      this.resultsCharts = response;
      this.setChartsData();
    },error=>{
      console.log(error);
    })
  }


  setRowsData(){
    this.rows[0].data = this.systemTest.current_filter.current_values;
    this.rows[0].data.savings = 0;
    this.rows[0].data.savings_porcent = 0;
    this.rows[0].color = '#f7cd42';

    this.rows[1].data = this.systemTest.current_filter.optimize_values;
    this.rows[1].data.savings = (this.rows[0].data.total_cost - this.rows[1].data.total_cost).toFixed(2);
    this.rows[1].data.savings_porcent = ((this.rows[1].data.savings * 100) / this.rows[0].data.total_cost).toFixed(2);

    this.rows[2].data = this.systemTest.proposed_filter.given_time_values;
    this.rows[2].data.savings = (this.rows[0].data.total_cost - this.rows[2].data.total_cost).toFixed(2);
    this.rows[2].data.savings_porcent = ((this.rows[2].data.savings * 100) / this.rows[0].data.total_cost).toFixed(2);

    this.rows[3].data = this.systemTest.proposed_filter.given_dp_values;
    this.rows[3].data.savings = (this.rows[0].data.total_cost - this.rows[3].data.total_cost).toFixed(2);
    this.rows[3].data.savings_porcent = ((this.rows[3].data.savings * 100) / this.rows[0].data.total_cost).toFixed(2);

    this.rows[4].data = this.systemTest.proposed_filter.optimize_values;
    this.rows[4].data.savings = (this.rows[0].data.total_cost - this.rows[4].data.total_cost).toFixed(2);
    this.rows[4].data.savings_porcent = ((this.rows[4].data.savings * 100) / this.rows[0].data.total_cost).toFixed(2);

    for (let index = 1; index < this.rows.length; index++) {
      if(this.rows[index].data.savings_porcent < 0){
        this.rows[index].color = '#cc0000';
      }
    }
  }

  setChartsData(){
    this.rows[0].graphData = this.resultsCharts.dp_vs_t_w_graph_current_filter;
    this.rows[1].graphData = this.resultsCharts.dp_vs_t_w_optimized_graph_current_filter;
    this.rows[2].graphData = this.resultsCharts.given_time_values_graph_proposed_filter;
    this.rows[3].graphData = this.resultsCharts.given_dp_values_graph_proposed_filter;
    this.rows[4].graphData = this.resultsCharts.dp_vs_t_w_optimized_graph_proposed_filter;
    this.chartData = this.rows[4].graphData;
    this.showMainGraphic = true;
    this.showGraphics = true;
  }

  setToggle(){
  }

  getModelFilters(){
    this.testService.getFilters().subscribe((response)=>{
     console.log(response);
     this.modelFilters = response;
    },error=>{
      console.log(error);
    })
  }

  setStep(s){
    if (s==5){
      this.calculateCosts();
      this.setData();
      this.loader = true;
    }
    
    this.step = s;
    this.footer_class = ['footer-text','footer-text','footer-text','footer-text','footer-text'];
    this.icon_class =  ['footer-icon','footer-icon','footer-icon','footer-icon','footer-icon'];
    this.footer_class[s-1] = 'footer-text-active';
    this.icon_class[s-1] = 'footer-icon-active';
    this.current_left_text = this.left_texts[s-2];
    this.current_right_text = this.right_texts[s-1];
  }

  setData(){
    var current_filter = Object.assign({}, this.current_filter);
    var proposed_filter = Object.assign({}, this.proposed_filter);
    var system =  Object.assign({}, this.system);

    current_filter.additional_costs = 
      this.current_filter.annual_cost+
      this.current_filter.replace_cost;

    delete current_filter.filter_name;
    delete current_filter.aditional_costs;
    delete current_filter.drive_efficiency;
    delete current_filter.fan_efficiency;
    delete current_filter.motor_efficiency;
    
    delete proposed_filter.filter_name;
    delete proposed_filter.drive_efficiency;
    delete proposed_filter.fan_efficiency;
    delete proposed_filter.motor_efficiency;
    
    this.data = {
      current_filter,
      proposed_filter,
      "airflow_per_filter": system.airflow_per_filter ,
      "backward_fan": system.backward_fan,
      "replacement_cycle": system.replacement_cycle,
      "energy_cost": system.energy_cost,
      "co2_emission": system.co2_emission,
      "co2_cost": 0,   
      "comments": system.comments,
      "total_airflow": system.total_airflow ,
      "days_per_month": system.days_per_month,
      "drive_efficiency": parseFloat(system.drive_efficiency),
      "fan_efficiency": parseFloat(system.fan_efficiency),
      "filters_number": system.filters_number,
      "forward_fan": system.forward_fan,
      "hours_per_day": system.hours_per_day,
      "industry_type": system.industry_type,
      "months_per_year": system.months_per_year,
      "motor_efficiency": parseFloat(system.motor_efficiency),
      "radial_fan": system.radial_fan,
      "variable_airflow": system.variable_airflow
    }
    this.sendData();
    // this.testService.getSystem(7).subscribe((response)=>{
    //   this.systemTest = response;
    //   console.log(this.systemTest);
      
    // },error=>{
    //   console.log(error);
    // })
  }

  sendData(){
    console.log(this.data);
    this.testService.calculate(this.data).subscribe((response)=>{
      this.testService.getSystem(response.id).subscribe((response2)=>{
        this.systemTest = response2;
        this.setRowsData();
        this.loader =false;
        this.getCharts(response.id);
      },error=>{
        this.loader =false;
        console.log(error);
      })
    },error=>{
      this.loader =false;
      console.log(error);
    })
  }

  changeValue(e,t){
    if (t==0){
      this.system.motor_efficiency = e;
    }
    if (t==1){
      this.system.drive_efficiency = e;
    }
    if (t==2){
      this.system.fan_efficiency = e;
    }
  }

  calculateCosts(){
    this.current_filter.replace_cost = 
    this.current_filter.aditional_costs[0]+
    this.current_filter.aditional_costs[1]+
    this.current_filter.aditional_costs[2]+
    this.current_filter.aditional_costs[3];

    this.current_filter.annual_cost = 
    this.current_filter.aditional_costs[4]+
    this.current_filter.aditional_costs[5]
  }

  setTypeIndustry(t){
    this.system.industry_type = t;
    this.btn_class = ['btn-deactive','btn-deactive','btn-deactive'];
    this.btn_class[t-1] = 'btn-active'
  }

  validateCoEmission(){
    if(this.system.co2_emission < 0){
      this.system.co2_emission = this.system.co2_emission * -1
    }
  }

  validateCost(){
    if(this.system.energy_cost < 0){
      this.system.energy_cost = this.system.energy_cost * -1
    }
  }

  validateMonth(){
    if(this.system.months_per_year > 12){
      this.system.months_per_year = 12;
    }
    if(this.system.months_per_year == 0){
      this.system.months_per_year = 1;
    }
    if(this.system.months_per_year < 0){
      this.system.months_per_year = this.system.months_per_year * -1 
    }
  }

  validateAirflow(){
    if(this.system.airflow_per_filter  < 1){
      this.system.airflow_per_filter  = this.system.airflow_per_filter  * -1
    }
  }

  validateCurrentAirflow(){
    if(this.system.total_airflow  < 1){
      this.system.total_airflow  = this.system.total_airflow  * -1
    }
  }

  validateDays(){
    if(this.system.days_per_month > 31){
      this.system.days_per_month = 31;
    }
    if(this.system.days_per_month == 0){
      this.system.days_per_month = 1;
    }
    if(this.system.days_per_month < 0){
      this.system.days_per_month = this.system.days_per_month * -1 
    }
  }

  validateHours(){
    if(this.system.hours_per_day > 24){
      this.system.hours_per_day = 24;
    }
    if(this.system.hours_per_day == 0){
      this.system.hours_per_day = 1;
    }
    if(this.system.hours_per_day < 0){
      this.system.hours_per_day = this.system.hours_per_day * -1 
    }
  }

  validateCycle(){

    if(this.system.replacement_cycle == 0){
      this.system.replacement_cycle = 1;
    }
    if(this.system.replacement_cycle < 0){
      this.system.replacement_cycle = this.system.replacement_cycle * -1 
    }
  }

  validateNumFilters(){
    if(this.system.filters_number == 0){
      this.system.filters_number = 1;
    }
    if(this.system.filters_number < 0){
      this.system.filters_number = this.system.filters_number * -1 
    }
  }

  validateAdditionalCost(i){
    if(this.current_filter.aditional_costs[i] < 0){
      this.current_filter.aditional_costs[i] = this.current_filter.aditional_costs[i] * -1
    }
  }

  validateHeight(){
    if(this.current_filter.height < 0){
      this.current_filter.height = this.current_filter.height * -1
    }
  }

  validateLength(){
    if(this.current_filter.length < 0){
      this.current_filter.length = this.current_filter.length * -1
    }
  }

  validateWidth(){
    if(this.current_filter.width < 0){
      this.current_filter.width = this.current_filter.width * -1
    }
  }

  setProposedFilter(e){
    var filter_aux = e.value;
    console.log(filter_aux);
    this.proposed_filter.fan_efficiency = filter_aux.efficiency;
    this.proposed_filter.motor_efficiency = filter_aux.efficiency;
    this.proposed_filter.drive_efficiency = filter_aux.efficiency;
    this.proposed_filter.model_filter = filter_aux.id;
    this.proposed_filter.manufacturer = filter_aux.manufacturer;
    this.proposed_filter.height = filter_aux.size_height;
    this.proposed_filter.width = filter_aux.size_width;
    this.proposed_filter.length = filter_aux.size_depth;
    this.proposed_filter.incinarable = filter_aux.incineratable;
    this.proposed_filter.part_number = filter_aux.part_number;
    this.proposed_filter.filter_type = filter_aux.type;
    this.proposed_filter.frame_type = filter_aux.frame_type;
    this.proposed_filter.ul_compliant = filter_aux.ul_rating;
    this.proposed_filter.name =  filter_aux.short_name
  }

  setFilterValues(e){
    console.log(e.value);
    var filter_aux = e.value;
    this.current_filter.fan_efficiency = filter_aux.efficiency;
    this.current_filter.motor_efficiency = filter_aux.efficiency;
    this.current_filter.drive_efficiency = filter_aux.efficiency;
    this.current_filter.model_filter = filter_aux.id;
    this.current_filter.manufacturer = filter_aux.manufacturer;
    this.current_filter.height = filter_aux.size_height;
    this.current_filter.width = filter_aux.size_width;
    this.current_filter.length = filter_aux.size_depth;
    this.current_filter.incinarable = filter_aux.incineratable;
    this.current_filter.part_number = filter_aux.part_number;
    this.current_filter.filter_type = filter_aux.type;
    this.current_filter.frame_type = filter_aux.frame_type;
    this.current_filter.ul_compliant = filter_aux.ul_rating;
    this.current_filter.name = filter_aux.short_name;  
  }

  

}
